# rosbag2

#### Description
Repository for implementing rosbag2 as described in its corresponding design article.

#### Software Architecture
Software architecture description

https://github.com/ros2/rosbag2.git

input:
```
rosbag2/
|-- CONTRIBUTING.md
|-- LICENSE
|-- NOTICE
|-- README.md
|-- docs
|-- ros2bag
|-- rosbag2
|-- rosbag2_compression
|-- rosbag2_converter_default_plugins
|-- rosbag2_cpp
|-- rosbag2_samples
|-- rosbag2_storage
|-- rosbag2_storage_default_plugins
|-- rosbag2_storage_evaluation
|-- rosbag2_test_common
|-- rosbag2_tests
|-- rosbag2_transport
|-- shared_queues_vendor
|-- sqlite3_vendor
`-- zstd_vendor
```

#### Installation

1.  Download RPM

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rosbag2/ros-foxy-ros-rosbag2-0.3.9-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rosbag2/ros-foxy-ros-rosbag2-0.3.9-2.oe2203.x86_64.rpm
```

2.  Install RPM

aarch64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rosbag2-0.3.9-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rosbag2-0.3.9-2.oe2203.x86_64.rpm
```

#### Instructions

Dependence installation:

```
sh /opt/ros/foxy/install_dependence.sh
```

Exit the following output file under the /opt/ros/foxy/ directory,Prove that the software installation is successful.

output:

```
./
|-- COLCON_IGNORE
|-- _local_setup_util_ps1.py
|-- _local_setup_util_sh.py
|-- bin
|-- cmake
|-- include
|-- lib
|-- lib64
|-- local_setup.bash
|-- local_setup.ps1
|-- local_setup.sh
|-- local_setup.zsh
|-- setup.bash
|-- setup.ps1
|-- setup.sh
|-- setup.zsh
|-- share
`-- src
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
