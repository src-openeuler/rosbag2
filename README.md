# rosbag2

#### 介绍
用于实现 rosbag2 的存储库，如其相应的设计文章中所述。

#### 软件架构
软件架构说明

https://github.com/ros2/rosbag2.git

文件内容:
```
rosbag2/
|-- CONTRIBUTING.md
|-- LICENSE
|-- NOTICE
|-- README.md
|-- docs
|-- ros2bag
|-- rosbag2
|-- rosbag2_compression
|-- rosbag2_converter_default_plugins
|-- rosbag2_cpp
|-- rosbag2_samples
|-- rosbag2_storage
|-- rosbag2_storage_default_plugins
|-- rosbag2_storage_evaluation
|-- rosbag2_test_common
|-- rosbag2_tests
|-- rosbag2_transport
|-- shared_queues_vendor
|-- sqlite3_vendor
`-- zstd_vendor
```
#### 安装教程

1. 下载RPM包

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rosbag2/ros-foxy-ros-rosbag2-0.3.9-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rosbag2/ros-foxy-ros-rosbag2-0.3.9-2.oe2203.x86_64.rpm
```

2. 安装rpm包

aarch64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rosbag2-0.3.9-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-rosbag2-0.3.9-2.oe2203.x86_64.rpm
```

#### 使用说明

依赖环境安装:

```
sh /opt/ros/foxy/install_dependence.sh
```

安装完成以后，在/opt/ros/foxy/目录下有如下输出,则表示安装成功。

输出:

```
./
|-- COLCON_IGNORE
|-- _local_setup_util_ps1.py
|-- _local_setup_util_sh.py
|-- bin
|-- cmake
|-- include
|-- lib
|-- lib64
|-- local_setup.bash
|-- local_setup.ps1
|-- local_setup.sh
|-- local_setup.zsh
|-- setup.bash
|-- setup.ps1
|-- setup.sh
|-- setup.zsh
|-- share
`-- src
```

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
